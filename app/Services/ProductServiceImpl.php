<?php

namespace App\Services;

use App\Repositories\ProductRepositoryImpl;

class ProductServiceImpl extends Service
{
    private $repo;
    public function __construct(ProductRepositoryImpl $repo)
    {
        $this->repo = $repo;
    }

    public function list($paginate)
    {
        return $this->repo->list($paginate);
    }

    public function all()
    {
        return $this->repo->all();
    }

    public function create($data)
    {
        return $this->repo->create($data);
    }

    public function update($data, $id)
    {
        return $this->repo->update($data,$id);
    }

    public function delete($id)
    {
        return $this->repo->delete($id);
    }

    public function show($id)
    {
        return $this->repo->show($id);
    }
}
