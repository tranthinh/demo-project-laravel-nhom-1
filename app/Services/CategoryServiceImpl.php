<?php

namespace App\Services;

use App\Repositories\CategoryRepositoryImpl;

class CategoryServiceImpl extends Service
{
    private $cateRepo;

    public function __construct(CategoryRepositoryImpl $cateRepo)
    {
        $this->cateRepo = $cateRepo;
    }

    public function list($paginate)
    {
        return $this->cateRepo->list($paginate);
    }

    public function all()
    {
        return $this->cateRepo->all();
    }

    public function create($data)
    {
        return $this->cateRepo->create($data);
    }

    public function update($data, $id)
    {
        return $this->cateRepo->update($data, $id);
    }

    public function delete($id)
    {
        return $this->cateRepo->delete($id);
    }

    public function show($id)
    {
        return $this->cateRepo->show($id);
    }
}
