<?php

namespace App\Services;

use App\Repositories\RoleRepositoryImpl;

class RoleServiceImpl extends Service
{
    private $Rolerepo;
    public function __construct(RoleRepositoryImpl $Rolerepo)
    {
        $this->Rolerepo = $Rolerepo;
    }

    public function list($paginate)
    {
        return $this->Rolerepo->list($paginate);
    }

    public function all()
    {
        return $this->Rolerepo->all();
    }

    public function create($data)
    {
        return $this->Rolerepo->create($data);
    }

    public function update($data, $id)
    {
        return $this->Rolerepo->update($data, $id);
    }

    public function delete($id)
    {
        return $this->Rolerepo->delete($id);
    }

    public function show($id)
    {
        return $this->Rolerepo->show($id);
    }
}
