<?php

namespace App\Services;

interface ServiceInterface
{
    public function list($paginate);
    public function all();
    public function create($data);
    public function update($data, $id);
    public function delete($id);
    public function show($id);
}
