<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;

class RoleRepositoryImpl extends Repository
{
    private $roles;
    public function __construct(Role $roles)
    {
        $this->roles = $roles;
    }

    public function list($paginate)
    {
        return $this->roles->paginate($paginate);
    }

    public function all()
    {
        return $this->roles->all();
    }

    public function create($data)
    {
        return $this->roles->create($data);
    }

    public function update($data, $id)
    {
        return $this->roles->findOrFail($id)->update($data);
    }

    public function delete($id)
    {
        return $this->roles->findOrFail($id)->delete();
    }

    public function show($id)
    {
        return $this->roles->findOrFail($id);
    }
}
