<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepositoryImpl extends Repository
{
    private $products;
    public function __construct(Product $products)
    {
        $this->products =$products;
    }

    public function list($paginate)
    {
        return $this->products->with("category")->search()->byCategoriesId()->latest("id")->paginate($paginate);
    }

    public function all()
    {
        return $this->products->all();
    }

    public function create($data)
    {
        return $this->products->create($data);
    }

    public function update($data, $id)
    {
        return $this->products->findOrFail($id)->update($data);
    }

    public function delete($id)
    {
        return $this->products->findOrFail($id)->delete();
    }

    public function show($id)
    {
        return $this->products->with("category")->findOrFail($id);
    }
}
