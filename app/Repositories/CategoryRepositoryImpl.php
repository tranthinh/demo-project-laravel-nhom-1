<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepositoryImpl extends Repository
{
    private $categories;

    public function __construct(Category $categories)
    {
        $this->categories = $categories;
    }

    public function list($paginate)
    {
        return $this->categories->sortable()->search()->latest()->paginate($paginate);
    }

    public function all()
    {
        return $this->categories->all();
    }

    public function create($data)
    {
        return $this->categories->create($data);
    }

    public function update($data, $id)
    {
        $category = $this->categories->find($id);
        $category->update($data);
        return $category;
    }

    public function delete($id)
    {
        $category = $this->categories->find($id);
        $category->delete();
        return $category;
    }

    public function show($id)
    {
        return $this->categories->find($id);
    }
}
