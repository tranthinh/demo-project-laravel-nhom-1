<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ["name", "image", "status"];
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    public function scopeSearch($query)
    {
        if (!empty(request()->search)) {
            return $query->where('name', 'like', '%' . request()->search . '%');
        }
    }
}
