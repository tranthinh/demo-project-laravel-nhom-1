<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        "name", "image", "price", "amount", "description", "status", "categories_id"
    ];
    public function category()
    {
        return $this->belongsTo(Category::class,"categories_id");
    }
    public function scopeSearch($query)
    {
        if(!empty(request()->search)){
            return $query->where("name","like","%".request()->search."%");
        }
    }
    public function scopefindByIdCate($query)
    {
        if(!empty(request()->categories_id)){
            return $query->where("categories_id",request()->categories_id);
        }
    }
}
